<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Autores';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1></h1>

    <p>
        <?= Html::a('Create Autores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => 'Mostrando registros',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'nacionalidad',
            'foto',

            [
                'label' => 'Image',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img("@web/imgs/$data->foto", ['alt' => $data->nombre]);
                }
            ],
                    /*
            [
                'label' => 'Image',
                'format' => 'raw',
                'value' => function($data){
                    $url = "@web/imgs/f2.jpg";
                    return Html::img($url, ['alt' => 'Rene Descartes']);
                }
            ],
                     * 
                     */
        ],
    ]); ?>
</div>
